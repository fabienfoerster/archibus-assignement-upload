# README #

This repository contains the code require for the Archibus assignement.

I choose to do the "Upload File" assignement.

I did the following bonus task :

* Store uploaded file in a Blob field of a database.
* Use ExtJs or Sencha Touch on the client.
* Add persistence.

Note :

To retrieve a file just type /file/{file_name}

I used MongoDB to store the file so be sure to have MongoDB up and running