package hello.files;

import com.mongodb.gridfs.GridFSDBFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

import java.io.InputStream;
import java.util.List;

/**
 * Created by foerster on 27/01/15.
 */
public class FileStorageDaoImpl implements FileStorageDao {

    @Autowired
    GridFsTemplate gridFsTemplate;

    public String store(InputStream inputStream, String fileName
                        ) {
        return this.gridFsTemplate
                .store(inputStream, fileName).getId()
                .toString();
    }

    public GridFSDBFile getById(String id) {
        return this.gridFsTemplate.findOne(new Query(Criteria.where("_id").is(
                id)));
    }

    public GridFSDBFile getByFilename(String fileName) {
        return gridFsTemplate.findOne(new Query(Criteria.where("filename").is(
                fileName)));
    }

    public GridFSDBFile retrive(String fileName) {
        return gridFsTemplate.findOne(
                new Query(Criteria.where("filename").is(fileName)));
    }

    public List findAll() {
        return gridFsTemplate.find(null);
    }

}
