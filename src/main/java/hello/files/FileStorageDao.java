package hello.files;

/**
 * Created by foerster on 27/01/15.
 */
import java.io.InputStream;
import java.util.List;

import com.mongodb.gridfs.GridFSDBFile;

public interface FileStorageDao {

    public String store(InputStream inputStream, String fileName
                        );

    public GridFSDBFile retrive(String fileName);

    public GridFSDBFile getById(String id);

    public GridFSDBFile getByFilename(String filename);

    public List findAll();

}
