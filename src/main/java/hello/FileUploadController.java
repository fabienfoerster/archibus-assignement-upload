package hello;

import java.io.*;

import com.mongodb.gridfs.GridFSDBFile;
import hello.files.FileStorageDao;
import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@Controller
public class FileUploadController {





    
    @RequestMapping(value="/upload", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }
    
    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@RequestParam("name") String name, 
            @RequestParam("file") MultipartFile file){

        ExtJSFormResult extjsFormResult = new ExtJSFormResult();
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();

                // initialise mongo context
                ApplicationContext context = new ClassPathXmlApplicationContext(
                        "spring-beans.xml");
                FileStorageDao fileStorageDao = (FileStorageDao) context.getBean("fileStorageDao");

                // store the file on mongo
                InputStream inputStream = new ByteArrayInputStream(bytes);

                fileStorageDao.store(inputStream,name);

                extjsFormResult.setSuccess(true);

                return extjsFormResult.toString();
            } catch (Exception e) {
                extjsFormResult.setSuccess(false);
                return extjsFormResult.toString();
            }
        } else {
            extjsFormResult.setSuccess(false);
            return extjsFormResult.toString();
        }
    }

    @RequestMapping(value = "/file/{file_name:.+}", method = RequestMethod.GET)
    public void getFile(
            @PathVariable("file_name") String fileName,
            HttpServletResponse response) {
        try {

            // initialise mongo context
            ApplicationContext context = new ClassPathXmlApplicationContext(
                    "spring-beans.xml");
            FileStorageDao fileStorageDao = (FileStorageDao) context.getBean("fileStorageDao");

            // retrieve the file from mongodb
            GridFSDBFile file = fileStorageDao.getByFilename(fileName);

            // file not found
            if(file == null){
                response.sendRedirect("../404.html");
                return;
            }
            IOUtils.copy(file.getInputStream(), response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }

    }


}
